import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas(emojis) {
	  const emojisWithBanana = emojis.map(x => x + "🍌");
	  
	  const p = document.createElement("p");
	  p.textContent = emojisWithBanana;
	  
	  const divWrapper = document.getElementById("emojis");
	  divWrapper.innerHTML = "";
	  
	  divWrapper.appendChild(p);
	  
	  return emojisWithBanana;	  
  }
}
